
package trabalho1ppd;

import java.awt.Color;
import javax.swing.JLabel;

public class Piece {
    
    private String name;
    private int position;
    private int force;
    private Color color;
    private JLabel label;

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
    public Piece(String name, int force, Color color, JLabel label){
        this.name = name;
        this.force = force;
        this.color = color;
        this.label = label;
    }
    
    public Piece(){
    }
    
    static Piece batle(Piece attacker, Piece deffender){
        // regras de uma batalha entre duas peças
        return attacker;
    }
    
}
