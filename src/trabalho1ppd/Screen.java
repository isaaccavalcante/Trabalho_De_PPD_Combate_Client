package trabalho1ppd;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.showConfirmDialog;

public class Screen extends javax.swing.JFrame {

    private InputStreamReader inputSR;
    private BufferedReader bufReader;
    PrintStream prtStream;

    private String name;
    private int age;
    private String color;

    private boolean firstConfigurationFinished = false;
    private boolean secondConfigurationFinished = false;
    private boolean isMyTurn = false;
    private boolean gameBegin = false;
    private boolean firstClick = false;

    private String gamePatern = "!!!@@@###$$$!@#$";
    private String pieceChoose = null;

    private ArrayList<Piece> pieces = new ArrayList<>();
    private Piece from = null;
    private Piece to = null;
    private Piece lastStateFrom = null;
    private Piece lastStateTo = null;
    private ArrayList<Piece> initialPieces = new ArrayList<>();
//    private ArrayList<Color> names;

    private int soldiers = 8;
    private int mines = 6;
    private int corporals = 5;
    private int sergeants = 4;
    private int liutenants = 4;
    private int subLiutenants = 4;
    private int captains = 3;
    private int majors = 2;
    private int colonels = 1;
    private int generals = 1;
    private int flags = 1;
    private int spies = 1;

    public Screen(String name, int age) {
        this.name = name;
        this.age = age;

        try {
            this.prtStream = new PrintStream(Application.socket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
        }

        Thread();
        initComponents();

        int j = 0;
        for (int i = 0; i < this.getContentPane().getComponentCount(); i++) {
            if (!this.getContentPane().getComponent(i).getName().equals("noMovimentLabel")) {
                JLabel movimentLabel = (JLabel) this.getContentPane().getComponent(i);
                movimentLabel.setText(Integer.toString(j));
                movimentLabel.setName(Integer.toString(j++));
                movimentLabel.addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        labelsListtener(evt);
                    }
                });
                Piece newPiece = new Piece();
                newPiece.setLabel(movimentLabel);
                pieces.add(newPiece);
            }
        }
    }

    private void Thread() {
        Thread t;
        t = new Thread(new Runnable() {

            String message;
            String[] comands;

            @Override
            public void run() {
                try {
                    inputSR = new InputStreamReader(Application.socket.getInputStream());
                    bufReader = new BufferedReader(inputSR);
                    while ((message = bufReader.readLine()) != null) {

                        if (message.length() >= 16) {
                            if (!(message.substring(0, 16).equals(gamePatern))) {
                                receivedMessage.setText(receivedMessage.getText() + message + "\n");
                            } else {
                                String comand = null;
                                comands = message.split("/");

//==============================Detecção de incosistências entre as duas aplicações==============================//
                                if (comands.length == 2) {
                                    String newName;
                                    if ("!".equals(comands[1])) {
                                        try {
                                            do {
                                                newName = showInputDialog(null, "Esse nome já está sendo usado. Digite outro.", "", PLAIN_MESSAGE);
                                            } while (newName.equals(name));
                                            name = newName;
                                            String oldColor = color;
                                            color = "red".equals(color) ? "blue" : "red";
                                            showMessageDialog(null, "Você não pode utilizar a cor " + oldColor + ", pois ela já está sendo usada. Por isso você vai usar a cor " + color, "", INFORMATION_MESSAGE);
                                        } catch (HeadlessException e) {
                                            System.exit(0);
                                        }
                                    }
                                    if ("&".equals(comands[1])) {
                                        String oldColor = color;
                                        color = "red".equals(color) ? "blue" : "red";
                                        showMessageDialog(null, "Você não pode utilizar a cor " + oldColor + ", pois ela já está sendo usada. Por isso você vai usar a cor " + color, "", INFORMATION_MESSAGE);
                                    }
                                    if ("$".equals(comands[1])) {
                                        do {
                                            newName = showInputDialog(null, "Esse nome já está sendo usado. Digite outro.", "", PLAIN_MESSAGE);
                                        } while (newName.equals(name));
                                        name = newName;
                                    }
                                    if ("reset".equals(comands[1])) {
                                        int resposta = showConfirmDialog(null, "Seu adversário está solicitando um reset de partida. Você aceita?", "", JOptionPane.YES_NO_OPTION);
                                        if (resposta == JOptionPane.YES_OPTION) {
                                            showMessageDialog(null, "A partida será resetada", "", INFORMATION_MESSAGE);
                                            pieces = initialPieces;
                                            setInitialGameState();
                                            comand = gamePatern + "/reset/@";
                                        } else if (resposta == JOptionPane.NO_OPTION) {
                                            comand = gamePatern + "/reset/!";
                                        }
                                    }
                                    if ("desistir".equals(comands[1])) {
                                        showMessageDialog(null, "Seu adversário desistiu. Você é o vencedor. Parabéns !!!", "", INFORMATION_MESSAGE);
                                        System.exit(0);
                                    }
                                }
//==============================Detecção de incosistências entre as duas aplicações==============================//

                                if (comands.length == 3) {
                                    if ("reset".equals(comands[1]) && comands[2].equals("!")) {
                                        showMessageDialog(null, "O adversário não aceitou o reset da partida", "", INFORMATION_MESSAGE);
                                    } else if ("reset".equals(comands[1]) && comands[2].equals("@")) {
                                        showMessageDialog(null, "O adversário aceitou o reset de partida e a mesma será resetada.", "", INFORMATION_MESSAGE);
                                        pieces = initialPieces;
                                        setInitialGameState();
                                    }

                                    if ("lastMove".equals(comands[1]) && comands[2].equals("!")) {
                                        showMessageDialog(null, "O adversário não aceitou a sua jogada", "", INFORMATION_MESSAGE);
                                        unDoLastMove();
                                    }

                                    if (comands[1].equals(name) && comands[2].equals(color)) {
                                        comand = gamePatern + "/!";
                                    } else if (comands[1].equals(name) && firstConfigurationFinished) {
                                        comand = gamePatern + "/$";
                                    } else if (comands[2].equals(color)) {
                                        comand = gamePatern + "/&";
                                    }

//==================================Comunicação entre as aplicações para decidir quem começará a partida==================================//
                                    if (comands[1].equals("idade")) {
                                        if (comands[2].equals("!") || comands[2].equals("@")) {
                                            isMyTurn = !comands[2].equals("!");
                                            if (!isMyTurn) {
                                                showMessageDialog(null, "Você é o segundo a jogar. Espere o adversário terminar o seu movimento", "", INFORMATION_MESSAGE);
                                            }
                                            gameBegin = true;
                                            pieces = initialPieces;
                                            setInitialGameState();
                                        } else {
                                            comand = gamePatern + "/idade/";
                                            isMyTurn = ((Integer.parseInt(comands[2]) - age) > 0) || isMyTurn;

                                            if (secondConfigurationFinished && ((Integer.parseInt(comands[2]) - age) == 0)) {
                                                isMyTurn = true;
                                            }

                                            if (isMyTurn) {
                                                showMessageDialog(null, "Você é o primeiro a jogar. Se já tiver terminado a distribuição pode fazer seu movimento", "", INFORMATION_MESSAGE);
                                                gameBegin = true;
                                                if (color != null) {
                                                    pieces = initialPieces;
                                                    setInitialGameState();
                                                }
                                                comand += "!";
                                            } else {
                                                comand += "@";
                                            }
                                        }
                                    }
//==================================Comunicação entre as aplicações para decidir quem começará a partida==================================//
                                }

//==============================Movimentação de peças==============================//
                                if (comands.length == 7) {
                                    isMyTurn = true;
                                    if (!verifyTurnValidation(comands)) {
                                        comand = gamePatern + "/lastMove/!";
                                    }
                                }
//==============================Movimentação de peças==============================//

                                if (comand != null) {
                                    prtStream.println(comand);
                                    prtStream.flush();
                                }

                            }
                        } else {
                            receivedMessage.setText(receivedMessage.getText() + message + "\n");
                        }
                    }

                } catch (IOException e) {
                    showMessageDialog(null, "Erro na conexão com o servidor", "", ERROR_MESSAGE);
                }
            }
        });

        t.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        receivedMessage = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        messageToSend = new javax.swing.JTextArea();
        sendButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        flagChoose = new javax.swing.JLabel();
        liutenantChoose = new javax.swing.JLabel();
        colonelChoose = new javax.swing.JLabel();
        subLiutenantChoose = new javax.swing.JLabel();
        majorChoose = new javax.swing.JLabel();
        sergeantChoose = new javax.swing.JLabel();
        captainChoose = new javax.swing.JLabel();
        corporalChoose = new javax.swing.JLabel();
        generalChoose = new javax.swing.JLabel();
        soldierChoose = new javax.swing.JLabel();
        spyChoose = new javax.swing.JLabel();
        mineChoose = new javax.swing.JLabel();
        flagLabelDescription = new javax.swing.JLabel();
        generalLabelDescription = new javax.swing.JLabel();
        colonelLabelDescription = new javax.swing.JLabel();
        majorLabelDescription = new javax.swing.JLabel();
        captainLabelDescription = new javax.swing.JLabel();
        lietenantLabelDescription = new javax.swing.JLabel();
        subLiutenantLabelDescription = new javax.swing.JLabel();
        sergeantLabelDescription = new javax.swing.JLabel();
        corporalLabelDescription = new javax.swing.JLabel();
        soldierLabelDescription = new javax.swing.JLabel();
        spyLabelDescription = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        mineLabelDescription = new javax.swing.JLabel();
        label99 = new javax.swing.JLabel();
        label98 = new javax.swing.JLabel();
        label97 = new javax.swing.JLabel();
        label96 = new javax.swing.JLabel();
        label95 = new javax.swing.JLabel();
        label94 = new javax.swing.JLabel();
        label93 = new javax.swing.JLabel();
        label92 = new javax.swing.JLabel();
        label91 = new javax.swing.JLabel();
        label90 = new javax.swing.JLabel();
        label89 = new javax.swing.JLabel();
        label88 = new javax.swing.JLabel();
        label87 = new javax.swing.JLabel();
        label86 = new javax.swing.JLabel();
        label85 = new javax.swing.JLabel();
        label84 = new javax.swing.JLabel();
        label83 = new javax.swing.JLabel();
        label82 = new javax.swing.JLabel();
        label81 = new javax.swing.JLabel();
        label80 = new javax.swing.JLabel();
        label79 = new javax.swing.JLabel();
        label78 = new javax.swing.JLabel();
        label77 = new javax.swing.JLabel();
        label76 = new javax.swing.JLabel();
        label75 = new javax.swing.JLabel();
        label74 = new javax.swing.JLabel();
        label73 = new javax.swing.JLabel();
        label72 = new javax.swing.JLabel();
        label71 = new javax.swing.JLabel();
        label70 = new javax.swing.JLabel();
        label69 = new javax.swing.JLabel();
        label68 = new javax.swing.JLabel();
        label67 = new javax.swing.JLabel();
        label66 = new javax.swing.JLabel();
        label65 = new javax.swing.JLabel();
        label64 = new javax.swing.JLabel();
        label63 = new javax.swing.JLabel();
        label62 = new javax.swing.JLabel();
        label61 = new javax.swing.JLabel();
        label60 = new javax.swing.JLabel();
        label59 = new javax.swing.JLabel();
        label58 = new javax.swing.JLabel();
        label57 = new javax.swing.JLabel();
        label56 = new javax.swing.JLabel();
        label55 = new javax.swing.JLabel();
        label54 = new javax.swing.JLabel();
        label53 = new javax.swing.JLabel();
        label52 = new javax.swing.JLabel();
        label51 = new javax.swing.JLabel();
        label50 = new javax.swing.JLabel();
        label49 = new javax.swing.JLabel();
        label48 = new javax.swing.JLabel();
        label47 = new javax.swing.JLabel();
        label46 = new javax.swing.JLabel();
        label45 = new javax.swing.JLabel();
        label44 = new javax.swing.JLabel();
        label43 = new javax.swing.JLabel();
        label42 = new javax.swing.JLabel();
        label41 = new javax.swing.JLabel();
        label40 = new javax.swing.JLabel();
        label39 = new javax.swing.JLabel();
        label38 = new javax.swing.JLabel();
        label37 = new javax.swing.JLabel();
        label36 = new javax.swing.JLabel();
        label35 = new javax.swing.JLabel();
        label34 = new javax.swing.JLabel();
        label33 = new javax.swing.JLabel();
        label32 = new javax.swing.JLabel();
        label31 = new javax.swing.JLabel();
        label30 = new javax.swing.JLabel();
        label29 = new javax.swing.JLabel();
        label28 = new javax.swing.JLabel();
        label27 = new javax.swing.JLabel();
        label26 = new javax.swing.JLabel();
        label25 = new javax.swing.JLabel();
        label24 = new javax.swing.JLabel();
        label23 = new javax.swing.JLabel();
        label22 = new javax.swing.JLabel();
        label21 = new javax.swing.JLabel();
        label20 = new javax.swing.JLabel();
        label19 = new javax.swing.JLabel();
        label18 = new javax.swing.JLabel();
        label17 = new javax.swing.JLabel();
        label16 = new javax.swing.JLabel();
        label15 = new javax.swing.JLabel();
        label14 = new javax.swing.JLabel();
        label13 = new javax.swing.JLabel();
        label12 = new javax.swing.JLabel();
        label11 = new javax.swing.JLabel();
        label10 = new javax.swing.JLabel();
        label9 = new javax.swing.JLabel();
        label8 = new javax.swing.JLabel();
        label7 = new javax.swing.JLabel();
        label6 = new javax.swing.JLabel();
        label5 = new javax.swing.JLabel();
        label4 = new javax.swing.JLabel();
        label3 = new javax.swing.JLabel();
        label2 = new javax.swing.JLabel();
        label1 = new javax.swing.JLabel();
        label0 = new javax.swing.JLabel();
        background = new javax.swing.JLabel();
        finishTurn1 = new javax.swing.JButton();
        blueChoose = new javax.swing.JButton();
        redChoose = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        logs = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1200, 1000));
        getContentPane().setLayout(null);

        jScrollPane1.setName("noMovimentLabel"); // NOI18N

        receivedMessage.setEditable(false);
        receivedMessage.setColumns(20);
        receivedMessage.setRows(5);
        receivedMessage.setName(""); // NOI18N
        jScrollPane1.setViewportView(receivedMessage);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(635, 6, 530, 332);

        jScrollPane2.setName("noMovimentLabel"); // NOI18N

        messageToSend.setColumns(20);
        messageToSend.setRows(5);
        messageToSend.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        messageToSend.setName(""); // NOI18N
        messageToSend.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                messageToSendKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(messageToSend);
        messageToSend.getAccessibleContext().setAccessibleName("");

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(635, 344, 530, 135);

        sendButton.setText("Enviar");
        sendButton.setName("noMovimentLabel"); // NOI18N
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendButtonActionPerformed(evt);
            }
        });
        getContentPane().add(sendButton);
        sendButton.setBounds(630, 480, 270, 41);

        jButton1.setText("Sair");
        jButton1.setName("noMovimentLabel"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(920, 480, 250, 41);

        flagChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/f.png"))); // NOI18N
        flagChoose.setName("noMovimentLabel"); // NOI18N
        flagChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                flagChooseMouseClicked(evt);
            }
        });
        getContentPane().add(flagChoose);
        flagChoose.setBounds(640, 550, 60, 60);

        liutenantChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/5.png"))); // NOI18N
        liutenantChoose.setName("noMovimentLabel"); // NOI18N
        liutenantChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                liutenantChooseMouseClicked(evt);
            }
        });
        getContentPane().add(liutenantChoose);
        liutenantChoose.setBounds(1030, 550, 60, 60);

        colonelChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/2.png"))); // NOI18N
        colonelChoose.setName("noMovimentLabel"); // NOI18N
        colonelChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                colonelChooseMouseClicked(evt);
            }
        });
        getContentPane().add(colonelChoose);
        colonelChoose.setBounds(790, 550, 60, 60);

        subLiutenantChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/6.png"))); // NOI18N
        subLiutenantChoose.setName("noMovimentLabel"); // NOI18N
        subLiutenantChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                subLiutenantChooseMouseClicked(evt);
            }
        });
        getContentPane().add(subLiutenantChoose);
        subLiutenantChoose.setBounds(1030, 650, 60, 60);

        majorChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/3.png"))); // NOI18N
        majorChoose.setName("noMovimentLabel"); // NOI18N
        majorChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                majorChooseMouseClicked(evt);
            }
        });
        getContentPane().add(majorChoose);
        majorChoose.setBounds(870, 550, 60, 60);

        sergeantChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/7.png"))); // NOI18N
        sergeantChoose.setName("noMovimentLabel"); // NOI18N
        sergeantChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sergeantChooseMouseClicked(evt);
            }
        });
        getContentPane().add(sergeantChoose);
        sergeantChoose.setBounds(640, 650, 60, 60);

        captainChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/4.png"))); // NOI18N
        captainChoose.setName("noMovimentLabel"); // NOI18N
        captainChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                captainChooseMouseClicked(evt);
            }
        });
        getContentPane().add(captainChoose);
        captainChoose.setBounds(950, 550, 60, 60);

        corporalChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/8.png"))); // NOI18N
        corporalChoose.setName("noMovimentLabel"); // NOI18N
        corporalChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                corporalChooseMouseClicked(evt);
            }
        });
        getContentPane().add(corporalChoose);
        corporalChoose.setBounds(720, 650, 60, 60);

        generalChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/1.png"))); // NOI18N
        generalChoose.setName("noMovimentLabel"); // NOI18N
        generalChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                generalChooseMouseClicked(evt);
            }
        });
        getContentPane().add(generalChoose);
        generalChoose.setBounds(720, 550, 60, 60);

        soldierChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/9.png"))); // NOI18N
        soldierChoose.setName("noMovimentLabel"); // NOI18N
        soldierChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                soldierChooseMouseClicked(evt);
            }
        });
        getContentPane().add(soldierChoose);
        soldierChoose.setBounds(790, 650, 60, 60);

        spyChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/s.png"))); // NOI18N
        spyChoose.setName("noMovimentLabel"); // NOI18N
        spyChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                spyChooseMouseClicked(evt);
            }
        });
        getContentPane().add(spyChoose);
        spyChoose.setBounds(870, 650, 60, 60);

        mineChoose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/b.png"))); // NOI18N
        mineChoose.setName("noMovimentLabel"); // NOI18N
        mineChoose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mineChooseMouseClicked(evt);
            }
        });
        getContentPane().add(mineChoose);
        mineChoose.setBounds(950, 650, 60, 60);

        flagLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        flagLabelDescription.setText("Bandeira");
        flagLabelDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        flagLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(flagLabelDescription);
        flagLabelDescription.setBounds(640, 530, 60, 16);

        generalLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        generalLabelDescription.setText("General");
        generalLabelDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        generalLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(generalLabelDescription);
        generalLabelDescription.setBounds(720, 530, 60, 16);

        colonelLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        colonelLabelDescription.setText("Coronel");
        colonelLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(colonelLabelDescription);
        colonelLabelDescription.setBounds(790, 530, 60, 16);

        majorLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        majorLabelDescription.setText("Major");
        majorLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(majorLabelDescription);
        majorLabelDescription.setBounds(870, 530, 60, 16);

        captainLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        captainLabelDescription.setText("Capitão");
        captainLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(captainLabelDescription);
        captainLabelDescription.setBounds(950, 530, 60, 16);

        lietenantLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lietenantLabelDescription.setText("Tenente");
        lietenantLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(lietenantLabelDescription);
        lietenantLabelDescription.setBounds(1030, 530, 60, 16);

        subLiutenantLabelDescription.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        subLiutenantLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        subLiutenantLabelDescription.setText("Subtenente");
        subLiutenantLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(subLiutenantLabelDescription);
        subLiutenantLabelDescription.setBounds(1030, 620, 60, 14);

        sergeantLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sergeantLabelDescription.setText("Sargento");
        sergeantLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(sergeantLabelDescription);
        sergeantLabelDescription.setBounds(640, 620, 60, 16);

        corporalLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        corporalLabelDescription.setText("Cabo");
        corporalLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(corporalLabelDescription);
        corporalLabelDescription.setBounds(720, 620, 60, 16);

        soldierLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        soldierLabelDescription.setText("Soldado");
        soldierLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(soldierLabelDescription);
        soldierLabelDescription.setBounds(790, 620, 60, 16);

        spyLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        spyLabelDescription.setText("Espião");
        spyLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(spyLabelDescription);
        spyLabelDescription.setBounds(870, 620, 60, 16);

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jSeparator3.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(jSeparator3);
        jSeparator3.setBounds(640, 620, 456, 2);

        mineLabelDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mineLabelDescription.setText("Minas");
        mineLabelDescription.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(mineLabelDescription);
        mineLabelDescription.setBounds(950, 620, 60, 16);

        label99.setText("jLabel1");
        label99.setName(""); // NOI18N
        getContentPane().add(label99);
        label99.setBounds(20, 10, 60, 60);

        label98.setText("jLabel1");
        label98.setName(""); // NOI18N
        getContentPane().add(label98);
        label98.setBounds(80, 10, 60, 60);

        label97.setText("jLabel1");
        label97.setName(""); // NOI18N
        getContentPane().add(label97);
        label97.setBounds(140, 10, 60, 60);

        label96.setText("jLabel1");
        label96.setName(""); // NOI18N
        getContentPane().add(label96);
        label96.setBounds(200, 10, 60, 60);

        label95.setText("jLabel1");
        label95.setName(""); // NOI18N
        getContentPane().add(label95);
        label95.setBounds(260, 10, 60, 60);

        label94.setText("jLabel1");
        label94.setName(""); // NOI18N
        getContentPane().add(label94);
        label94.setBounds(320, 10, 60, 60);

        label93.setText("jLabel1");
        label93.setName(""); // NOI18N
        getContentPane().add(label93);
        label93.setBounds(380, 10, 60, 60);

        label92.setText("jLabel1");
        label92.setName(""); // NOI18N
        getContentPane().add(label92);
        label92.setBounds(440, 10, 60, 60);

        label91.setText("jLabel1");
        label91.setName(""); // NOI18N
        getContentPane().add(label91);
        label91.setBounds(500, 10, 60, 60);

        label90.setText("jLabel1");
        label90.setName(""); // NOI18N
        getContentPane().add(label90);
        label90.setBounds(560, 10, 60, 60);

        label89.setText("jLabel1");
        label89.setName(""); // NOI18N
        getContentPane().add(label89);
        label89.setBounds(20, 70, 60, 60);

        label88.setText("jLabel1");
        label88.setName(""); // NOI18N
        getContentPane().add(label88);
        label88.setBounds(80, 70, 60, 60);

        label87.setText("jLabel1");
        label87.setName(""); // NOI18N
        getContentPane().add(label87);
        label87.setBounds(140, 70, 60, 60);

        label86.setText("jLabel1");
        label86.setName(""); // NOI18N
        getContentPane().add(label86);
        label86.setBounds(200, 70, 60, 60);

        label85.setText("jLabel1");
        label85.setName(""); // NOI18N
        getContentPane().add(label85);
        label85.setBounds(260, 70, 60, 60);

        label84.setText("jLabel1");
        label84.setName(""); // NOI18N
        getContentPane().add(label84);
        label84.setBounds(320, 70, 60, 60);

        label83.setText("jLabel1");
        label83.setName(""); // NOI18N
        getContentPane().add(label83);
        label83.setBounds(380, 70, 60, 60);

        label82.setText("jLabel1");
        label82.setName(""); // NOI18N
        getContentPane().add(label82);
        label82.setBounds(440, 70, 60, 60);

        label81.setText("jLabel1");
        label81.setName(""); // NOI18N
        getContentPane().add(label81);
        label81.setBounds(500, 70, 60, 60);

        label80.setText("jLabel1");
        label80.setName(""); // NOI18N
        getContentPane().add(label80);
        label80.setBounds(560, 70, 60, 60);

        label79.setText("jLabel1");
        label79.setName(""); // NOI18N
        getContentPane().add(label79);
        label79.setBounds(20, 130, 60, 60);

        label78.setText("jLabel1");
        label78.setName(""); // NOI18N
        getContentPane().add(label78);
        label78.setBounds(80, 130, 60, 60);

        label77.setText("jLabel1");
        label77.setName(""); // NOI18N
        getContentPane().add(label77);
        label77.setBounds(140, 130, 60, 60);

        label76.setText("jLabel1");
        label76.setName(""); // NOI18N
        getContentPane().add(label76);
        label76.setBounds(200, 130, 60, 60);

        label75.setText("jLabel1");
        label75.setName(""); // NOI18N
        getContentPane().add(label75);
        label75.setBounds(260, 130, 60, 60);

        label74.setText("jLabel1");
        label74.setName(""); // NOI18N
        getContentPane().add(label74);
        label74.setBounds(320, 130, 60, 60);

        label73.setText("jLabel1");
        label73.setName(""); // NOI18N
        getContentPane().add(label73);
        label73.setBounds(380, 130, 60, 60);

        label72.setText("jLabel1");
        label72.setName(""); // NOI18N
        getContentPane().add(label72);
        label72.setBounds(440, 130, 60, 60);

        label71.setText("jLabel1");
        label71.setName(""); // NOI18N
        getContentPane().add(label71);
        label71.setBounds(500, 130, 60, 60);

        label70.setText("jLabel1");
        label70.setName(""); // NOI18N
        getContentPane().add(label70);
        label70.setBounds(560, 130, 60, 60);

        label69.setText("jLabel1");
        label69.setName(""); // NOI18N
        getContentPane().add(label69);
        label69.setBounds(20, 190, 60, 60);

        label68.setText("jLabel1");
        label68.setName(""); // NOI18N
        getContentPane().add(label68);
        label68.setBounds(80, 190, 60, 60);

        label67.setText("jLabel1");
        label67.setName(""); // NOI18N
        getContentPane().add(label67);
        label67.setBounds(140, 190, 60, 60);

        label66.setText("jLabel1");
        label66.setName(""); // NOI18N
        getContentPane().add(label66);
        label66.setBounds(200, 190, 60, 60);

        label65.setText("jLabel1");
        label65.setName(""); // NOI18N
        getContentPane().add(label65);
        label65.setBounds(260, 190, 60, 60);

        label64.setText("jLabel1");
        label64.setName(""); // NOI18N
        getContentPane().add(label64);
        label64.setBounds(320, 190, 60, 60);

        label63.setText("jLabel1");
        label63.setName(""); // NOI18N
        getContentPane().add(label63);
        label63.setBounds(380, 190, 60, 60);

        label62.setText("jLabel1");
        label62.setName(""); // NOI18N
        getContentPane().add(label62);
        label62.setBounds(440, 190, 60, 60);

        label61.setText("jLabel1");
        label61.setName(""); // NOI18N
        getContentPane().add(label61);
        label61.setBounds(500, 190, 60, 60);

        label60.setText("jLabel1");
        label60.setName(""); // NOI18N
        getContentPane().add(label60);
        label60.setBounds(560, 190, 60, 60);

        label59.setText("jLabel1");
        label59.setName(""); // NOI18N
        getContentPane().add(label59);
        label59.setBounds(20, 250, 60, 60);

        label58.setText("jLabel1");
        label58.setName(""); // NOI18N
        getContentPane().add(label58);
        label58.setBounds(80, 250, 60, 60);

        label57.setText("jLabel1");
        label57.setName(""); // NOI18N
        getContentPane().add(label57);
        label57.setBounds(140, 250, 60, 60);

        label56.setText("jLabel1");
        label56.setName(""); // NOI18N
        getContentPane().add(label56);
        label56.setBounds(200, 250, 60, 60);

        label55.setText("jLabel1");
        label55.setName(""); // NOI18N
        getContentPane().add(label55);
        label55.setBounds(260, 250, 60, 60);

        label54.setText("jLabel1");
        label54.setName(""); // NOI18N
        getContentPane().add(label54);
        label54.setBounds(320, 250, 60, 60);

        label53.setText("jLabel1");
        label53.setName(""); // NOI18N
        getContentPane().add(label53);
        label53.setBounds(380, 250, 60, 60);

        label52.setText("jLabel1");
        label52.setName(""); // NOI18N
        getContentPane().add(label52);
        label52.setBounds(440, 250, 60, 60);

        label51.setText("jLabel1");
        label51.setName(""); // NOI18N
        getContentPane().add(label51);
        label51.setBounds(500, 250, 60, 60);

        label50.setText("jLabel1");
        label50.setName(""); // NOI18N
        getContentPane().add(label50);
        label50.setBounds(560, 250, 60, 60);

        label49.setText("jLabel1");
        label49.setName(""); // NOI18N
        getContentPane().add(label49);
        label49.setBounds(20, 310, 60, 60);

        label48.setText("jLabel1");
        label48.setName(""); // NOI18N
        getContentPane().add(label48);
        label48.setBounds(80, 310, 60, 60);

        label47.setText("jLabel1");
        label47.setName(""); // NOI18N
        getContentPane().add(label47);
        label47.setBounds(140, 310, 60, 60);

        label46.setText("jLabel1");
        label46.setName(""); // NOI18N
        getContentPane().add(label46);
        label46.setBounds(200, 310, 60, 60);

        label45.setText("jLabel1");
        label45.setName(""); // NOI18N
        getContentPane().add(label45);
        label45.setBounds(260, 310, 60, 60);

        label44.setText("jLabel1");
        label44.setName(""); // NOI18N
        getContentPane().add(label44);
        label44.setBounds(320, 310, 60, 60);

        label43.setText("jLabel1");
        label43.setName(""); // NOI18N
        getContentPane().add(label43);
        label43.setBounds(380, 310, 60, 60);

        label42.setText("jLabel1");
        label42.setName(""); // NOI18N
        getContentPane().add(label42);
        label42.setBounds(440, 310, 60, 60);

        label41.setText("jLabel1");
        label41.setName(""); // NOI18N
        getContentPane().add(label41);
        label41.setBounds(500, 310, 60, 60);

        label40.setText("jLabel1");
        label40.setName(""); // NOI18N
        getContentPane().add(label40);
        label40.setBounds(560, 310, 60, 60);

        label39.setText("jLabel1");
        label39.setName(""); // NOI18N
        getContentPane().add(label39);
        label39.setBounds(20, 370, 60, 60);

        label38.setText("jLabel1");
        label38.setName(""); // NOI18N
        getContentPane().add(label38);
        label38.setBounds(80, 370, 60, 60);

        label37.setText("jLabel1");
        label37.setName(""); // NOI18N
        getContentPane().add(label37);
        label37.setBounds(140, 370, 60, 60);

        label36.setText("jLabel1");
        label36.setName(""); // NOI18N
        getContentPane().add(label36);
        label36.setBounds(200, 370, 60, 60);

        label35.setText("jLabel1");
        label35.setName(""); // NOI18N
        getContentPane().add(label35);
        label35.setBounds(260, 370, 60, 60);

        label34.setText("jLabel1");
        label34.setName(""); // NOI18N
        getContentPane().add(label34);
        label34.setBounds(320, 370, 60, 60);

        label33.setText("jLabel1");
        label33.setName(""); // NOI18N
        getContentPane().add(label33);
        label33.setBounds(380, 370, 60, 60);

        label32.setText("jLabel1");
        label32.setName(""); // NOI18N
        getContentPane().add(label32);
        label32.setBounds(440, 370, 60, 60);

        label31.setText("jLabel1");
        label31.setName(""); // NOI18N
        getContentPane().add(label31);
        label31.setBounds(500, 370, 60, 60);

        label30.setText("jLabel1");
        label30.setName(""); // NOI18N
        getContentPane().add(label30);
        label30.setBounds(560, 370, 60, 60);

        label29.setText("jLabel1");
        label29.setName(""); // NOI18N
        getContentPane().add(label29);
        label29.setBounds(20, 430, 60, 60);

        label28.setText("jLabel1");
        label28.setName(""); // NOI18N
        getContentPane().add(label28);
        label28.setBounds(80, 430, 60, 60);

        label27.setText("jLabel1");
        label27.setName(""); // NOI18N
        getContentPane().add(label27);
        label27.setBounds(140, 430, 60, 60);

        label26.setText("jLabel1");
        label26.setName(""); // NOI18N
        getContentPane().add(label26);
        label26.setBounds(200, 430, 60, 60);

        label25.setText("jLabel1");
        label25.setName(""); // NOI18N
        getContentPane().add(label25);
        label25.setBounds(260, 430, 60, 60);

        label24.setText("jLabel1");
        label24.setName(""); // NOI18N
        getContentPane().add(label24);
        label24.setBounds(320, 430, 60, 60);

        label23.setText("jLabel1");
        label23.setName(""); // NOI18N
        getContentPane().add(label23);
        label23.setBounds(380, 430, 60, 60);

        label22.setText("jLabel1");
        label22.setName(""); // NOI18N
        getContentPane().add(label22);
        label22.setBounds(440, 430, 60, 60);

        label21.setText("jLabel1");
        label21.setName(""); // NOI18N
        getContentPane().add(label21);
        label21.setBounds(500, 430, 60, 60);

        label20.setText("jLabel1");
        label20.setName(""); // NOI18N
        getContentPane().add(label20);
        label20.setBounds(560, 430, 60, 60);

        label19.setText("jLabel1");
        label19.setName(""); // NOI18N
        getContentPane().add(label19);
        label19.setBounds(20, 490, 60, 60);

        label18.setText("jLabel1");
        label18.setName(""); // NOI18N
        getContentPane().add(label18);
        label18.setBounds(80, 490, 60, 60);

        label17.setText("jLabel1");
        label17.setName(""); // NOI18N
        getContentPane().add(label17);
        label17.setBounds(140, 490, 60, 60);

        label16.setText("jLabel1");
        label16.setName(""); // NOI18N
        getContentPane().add(label16);
        label16.setBounds(200, 490, 60, 60);

        label15.setText("jLabel1");
        label15.setName(""); // NOI18N
        getContentPane().add(label15);
        label15.setBounds(260, 490, 60, 60);

        label14.setText("jLabel1");
        label14.setName(""); // NOI18N
        getContentPane().add(label14);
        label14.setBounds(320, 490, 60, 60);

        label13.setText("jLabel1");
        label13.setName(""); // NOI18N
        getContentPane().add(label13);
        label13.setBounds(380, 490, 60, 60);

        label12.setText("jLabel1");
        label12.setName(""); // NOI18N
        getContentPane().add(label12);
        label12.setBounds(440, 490, 60, 60);

        label11.setText("jLabel1");
        label11.setName(""); // NOI18N
        getContentPane().add(label11);
        label11.setBounds(500, 490, 60, 60);

        label10.setText("jLabel1");
        label10.setName(""); // NOI18N
        getContentPane().add(label10);
        label10.setBounds(560, 490, 60, 60);

        label9.setText("jLabel1");
        label9.setName(""); // NOI18N
        getContentPane().add(label9);
        label9.setBounds(20, 550, 60, 60);

        label8.setText("jLabel1");
        label8.setName(""); // NOI18N
        getContentPane().add(label8);
        label8.setBounds(80, 550, 60, 60);

        label7.setText("jLabel1");
        label7.setName(""); // NOI18N
        getContentPane().add(label7);
        label7.setBounds(140, 550, 60, 60);

        label6.setText("jLabel1");
        label6.setName(""); // NOI18N
        getContentPane().add(label6);
        label6.setBounds(200, 550, 60, 60);

        label5.setText("jLabel1");
        label5.setName(""); // NOI18N
        getContentPane().add(label5);
        label5.setBounds(260, 550, 60, 60);

        label4.setText("jLabel1");
        label4.setName(""); // NOI18N
        getContentPane().add(label4);
        label4.setBounds(320, 550, 60, 60);

        label3.setText("jLabel1");
        label3.setName(""); // NOI18N
        getContentPane().add(label3);
        label3.setBounds(380, 550, 60, 60);

        label2.setText("jLabel1");
        label2.setName(""); // NOI18N
        getContentPane().add(label2);
        label2.setBounds(440, 550, 60, 60);

        label1.setText("jLabel1");
        label1.setName(""); // NOI18N
        getContentPane().add(label1);
        label1.setBounds(500, 550, 60, 60);

        label0.setText("jLabel1");
        label0.setName(""); // NOI18N
        getContentPane().add(label0);
        label0.setBounds(560, 550, 60, 60);

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/trabalho1ppd/newgrid2.png"))); // NOI18N
        background.setName("noMovimentLabel"); // NOI18N
        getContentPane().add(background);
        background.setBounds(10, 10, 620, 600);

        finishTurn1.setText("Iniciar jogo");
        finishTurn1.setName("noMovimentLabel"); // NOI18N
        finishTurn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finishTurn1ActionPerformed(evt);
            }
        });
        getContentPane().add(finishTurn1);
        finishTurn1.setBounds(10, 610, 620, 110);

        blueChoose.setBackground(new java.awt.Color(255, 255, 255));
        blueChoose.setForeground(new java.awt.Color(0, 0, 255));
        blueChoose.setText("Azul");
        blueChoose.setName("noMovimentLabel"); // NOI18N
        blueChoose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blueChooseActionPerformed(evt);
            }
        });
        getContentPane().add(blueChoose);
        blueChoose.setBounds(1100, 630, 70, 70);

        redChoose.setBackground(new java.awt.Color(255, 255, 255));
        redChoose.setForeground(new java.awt.Color(255, 0, 0));
        redChoose.setText("Vermelho");
        redChoose.setName("noMovimentLabel"); // NOI18N
        redChoose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redChooseActionPerformed(evt);
            }
        });
        getContentPane().add(redChoose);
        redChoose.setBounds(1100, 530, 70, 70);

        jScrollPane3.setName("noMovimentLabel"); // NOI18N

        logs.setColumns(20);
        logs.setRows(5);
        jScrollPane3.setViewportView(logs);

        getContentPane().add(jScrollPane3);
        jScrollPane3.setBounds(634, 524, 460, 190);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendButtonActionPerformed
        sendMessage();
    }//GEN-LAST:event_sendButtonActionPerformed

    private void messageToSendKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_messageToSendKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            sendMessage();
        }
    }//GEN-LAST:event_messageToSendKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            int resposta = showConfirmDialog(null, "Ao sair você estará desistindo da partida. Tem certeza disso?", "", JOptionPane.YES_NO_OPTION);
            if (resposta == JOptionPane.YES_OPTION) {
                String comand = gamePatern + "/desistir";
                prtStream.println(comand);
                prtStream.flush();
                Application.socket.close();
                System.exit(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void flagChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_flagChooseMouseClicked
        if (firstConfigurationFinished) {
            if (flags > 0 && pieceChoose == null) {
                flags--;
                pieceChoose = "flag";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_flagChooseMouseClicked

    private void redChooseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redChooseActionPerformed

        String comand = gamePatern;
        if (!firstConfigurationFinished) {
            color = "red";
            comand += "/" + name + "/" + color;

            firstConfigurationFinished = true;
            blueChoose.setEnabled(false);
            redChoose.setEnabled(false);

            showMessageDialog(null, "Cor definida. Pode começar a distribuir as peças", "", INFORMATION_MESSAGE);
        } else {
            comand += "/reset";
        }

        prtStream.println(comand);
        prtStream.flush();

    }//GEN-LAST:event_redChooseActionPerformed

    private void blueChooseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blueChooseActionPerformed

        String comand = gamePatern;
        if (!firstConfigurationFinished) {
            color = "blue";
            comand += "/" + name + "/" + color;

            firstConfigurationFinished = true;
            blueChoose.setEnabled(false);
            redChoose.setEnabled(false);

            showMessageDialog(null, "Cor definida. Pode começar a distribuir as peças", "", INFORMATION_MESSAGE);
        } else {
            comand += "/reset";
        }
        prtStream.println(comand);
        prtStream.flush();
    }//GEN-LAST:event_blueChooseActionPerformed

    private void finishTurn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finishTurn1ActionPerformed
        String comand = gamePatern;

        if (!secondConfigurationFinished) {
            if (firstConfigurationFinished) {
//                if (
//                    soldiers > 0 || corporals > 0 || sergeants > 0 || subLiutenants > 0 || liutenants > 0 || captains > 0 
//                    || majors > 0 || colonels > 0 || generals > 0 || mines > 0 || flags > 0 || spies > 0) {
//                    showMessageDialog(null, "Você deve distribuir TODAS as peças no tabuleiro", "", INFORMATION_MESSAGE);
//                } else {
                comand += "/idade/";
                comand += Integer.toString(age);

                prtStream.println(comand);
                prtStream.flush();
                secondConfigurationFinished = true;

                finishTurn1.setText("Finalizar turno");
                if (!isMyTurn) {
                    showMessageDialog(null, "Aguardando o outro jogador clicar em Iniciar jogo. O jogo irá iniciar em breve", "", INFORMATION_MESSAGE);
                }

                for (int i = 0; i < pieces.size(); i++) {
                    Piece newPiece = new Piece(pieces.get(i).getName(), pieces.get(i).getForce(), pieces.get(i).getColor(), pieces.get(i).getLabel());
                    initialPieces.add(newPiece);
                }

//                }
            } else {
                showMessageDialog(null, "Você só poderá iniciar o jogo quando definir a sua cor", "", INFORMATION_MESSAGE);
            }
        } else {
            if (!gameBegin) {
                showMessageDialog(null, "Aguardando o outro jogador clicar em 'Iniciar jogo'. O jogo irá iniciar em breve", "", INFORMATION_MESSAGE);
            } else {
                comand = gamePatern + "/name/" + from.getName() + "/from/" + Integer.toString(99 - Integer.parseInt(from.getLabel().getName())) + "/to/" + Integer.toString(99 - Integer.parseInt(to.getLabel().getName()));

                prtStream.println(comand);
                prtStream.flush();
            }
        }

    }//GEN-LAST:event_finishTurn1ActionPerformed

    private void generalChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generalChooseMouseClicked
        if (firstConfigurationFinished) {
            if (generals > 0 && pieceChoose == null) {
                generals--;
                pieceChoose = "general";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_generalChooseMouseClicked

    private void colonelChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_colonelChooseMouseClicked
        if (firstConfigurationFinished) {
            if (colonels > 0 && pieceChoose == null) {
                colonels--;
                pieceChoose = "colonel";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_colonelChooseMouseClicked

    private void majorChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_majorChooseMouseClicked
        if (firstConfigurationFinished) {
            if (majors > 0 && pieceChoose == null) {
                majors--;
                pieceChoose = "major";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_majorChooseMouseClicked

    private void captainChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_captainChooseMouseClicked
        if (firstConfigurationFinished) {
            if (captains > 0 && pieceChoose == null) {
                captains--;
                pieceChoose = "captain";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_captainChooseMouseClicked

    private void liutenantChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_liutenantChooseMouseClicked
        if (firstConfigurationFinished) {
            if (liutenants > 0 && pieceChoose == null) {
                liutenants--;
                pieceChoose = "liutenant";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_liutenantChooseMouseClicked

    private void subLiutenantChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_subLiutenantChooseMouseClicked
        if (firstConfigurationFinished) {
            if (subLiutenants > 0 && pieceChoose == null) {
                subLiutenants--;
                pieceChoose = "subLiutenant";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_subLiutenantChooseMouseClicked

    private void sergeantChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sergeantChooseMouseClicked
        if (firstConfigurationFinished) {
            if (sergeants > 0 && pieceChoose == null) {
                sergeants--;
                pieceChoose = "sergeant";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_sergeantChooseMouseClicked

    private void corporalChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_corporalChooseMouseClicked
        if (firstConfigurationFinished) {
            if (corporals > 0 && pieceChoose == null) {
                corporals--;
                pieceChoose = "corporal";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_corporalChooseMouseClicked

    private void soldierChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_soldierChooseMouseClicked
        if (firstConfigurationFinished) {
            if (soldiers > 0 && pieceChoose == null) {
                soldiers--;
                pieceChoose = "soldier";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_soldierChooseMouseClicked

    private void spyChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_spyChooseMouseClicked
        if (firstConfigurationFinished) {
            if (spies > 0 && pieceChoose == null) {
                spies--;
                pieceChoose = "spy";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_spyChooseMouseClicked

    private void mineChooseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mineChooseMouseClicked
        if (firstConfigurationFinished) {
            if (mines > 0 && pieceChoose == null) {
                mines--;
                pieceChoose = "mine";
            }
        } else {
            showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_mineChooseMouseClicked

    private void labelsListtener(java.awt.event.MouseEvent evt) {
        JLabel fonte = ((JLabel) evt.getSource());
        String source = null;

        if (!gameBegin) {
            if (firstConfigurationFinished && !secondConfigurationFinished && Integer.parseInt(fonte.getName()) < 60) {
                showMessageDialog(null, "Você não pode colocar a peça nessa posição agora.", "", INFORMATION_MESSAGE);
            } else {
                int force = 0;
                if (pieceChoose != null) {
                    if (pieceChoose.equals("flag")) {
                        source = "/trabalho1ppd/f.png";
                    }
                    if (pieceChoose.equals("general")) {
                        source = "/trabalho1ppd/1.png";
                        force = 10;
                    }
                    if (pieceChoose.equals("colonel")) {
                        source = "/trabalho1ppd/2.png";
                        force = 9;
                    }
                    if (pieceChoose.equals("major")) {
                        source = "/trabalho1ppd/3.png";
                        force = 8;
                    }
                    if (pieceChoose.equals("captain")) {
                        source = "/trabalho1ppd/4.png";
                        force = 7;
                    }
                    if (pieceChoose.equals("liutenant")) {
                        source = "/trabalho1ppd/5.png";
                        force = 6;
                    }
                    if (pieceChoose.equals("subLiutenant")) {
                        source = "/trabalho1ppd/6.png";
                        force = 5;
                    }
                    if (pieceChoose.equals("sergeant")) {
                        source = "/trabalho1ppd/7.png";
                        force = 4;
                    }
                    if (pieceChoose.equals("corporal")) {
                        source = "/trabalho1ppd/8.png";
                        force = 3;
                    }
                    if (pieceChoose.equals("soldier")) {
                        source = "/trabalho1ppd/9.png";
                        force = 2;
                    }
                    if (pieceChoose.equals("spy")) {
                        source = "/trabalho1ppd/s.png";
                        force = 1;
                    }
                    if (pieceChoose.equals("mine")) {
                        source = "/trabalho1ppd/b.png";
                    }
                }
                if (firstConfigurationFinished) {

                    Piece piece = pieces.get(Integer.parseInt(fonte.getName()));
                    JLabel label = piece.getLabel();
                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource(source)));
                    piece.setLabel(label);
                    piece.setName(pieceChoose);
                    piece.setForce(force);
                    piece.setColor(color.equals("red") ? Color.RED : Color.BLUE);
                    pieces.set(Integer.parseInt(fonte.getName()), piece);
//                    if (source != null) {
//                        // #TODO SÓ POSSO SETAR O ÍCONE DEPOIS DE FAZER AS VERIFICAÇÒES DE BATALHAS E POSSIBILIDADES DE MOVIMENTO
//                        fonte.setIcon(new javax.swing.ImageIcon(getClass().getResource(source)));
//                    }
                } else {
                    showMessageDialog(null, "Defina sua cor para começar a dispor as peças", "", INFORMATION_MESSAGE);
                }
            }
        } else if (isMyTurn) {
            if (!firstClick) {
                from = pieces.get(Integer.parseInt(fonte.getName()));
                firstClick = true;
                if ((from.getColor() == Color.RED && !color.equals("red"))
                        || (from.getColor() == Color.BLUE && !color.equals("blue"))) {
                    from = null;
                    showMessageDialog(null, "Você não pode mexer nas peças do seu adversário", "", INFORMATION_MESSAGE);
                }
            } else {
                to = pieces.get(Integer.parseInt(fonte.getName()));
                JLabel newLabelTo = new JLabel();
                
                newLabelTo.setOpaque(to.getLabel().isOpaque());
                newLabelTo.setIcon(to.getLabel().getIcon());
                newLabelTo.setBackground(to.getLabel().getBackground());
                newLabelTo.setName(to.getLabel().getName());
                lastStateTo = new Piece(to.getName(), to.getForce(), to.getColor(), newLabelTo);
                lastStateTo.setPosition(to.getPosition());

                to.getLabel().setIcon(from.getLabel().getIcon());
                to.getLabel().setOpaque(true);
                to.getLabel().setBackground(from.getColor());
                to.setName(from.getName());
                to.setColor(from.getColor());
                to.setForce(from.getForce());

                JLabel newLabelFrom = new JLabel();
                
                newLabelFrom.setOpaque(from.getLabel().isOpaque());
                newLabelFrom.setIcon(from.getLabel().getIcon());
                newLabelFrom.setBackground(from.getLabel().getBackground());
                newLabelFrom.setName(from.getLabel().getName());
                lastStateFrom = new Piece(from.getName(), from.getForce(), from.getColor(), newLabelFrom);
                lastStateFrom.setPosition(from.getPosition());

                from.getLabel().setOpaque(false);
                from.getLabel().setBackground(null);
                from.getLabel().setIcon(null);
                firstClick = false;
                isMyTurn = false;
//                from = null;
            }
        } else {
            showMessageDialog(null, "Espere seu turno", "", INFORMATION_MESSAGE);
        }
        pieceChoose = null;

//        pieceChoosed = new Piece("flag", );
    }

    private void unDoLastMove() {
        to.getLabel().setIcon(lastStateTo.getLabel().getIcon());
        to.getLabel().setOpaque(lastStateTo.getLabel().isOpaque());
        to.getLabel().setBackground(lastStateTo.getColor());
        to.setName(lastStateTo.getName());
        to.setColor(lastStateTo.getColor());
        to.setForce(lastStateTo.getForce());

        from.getLabel().setIcon(lastStateFrom.getLabel().getIcon());
        from.getLabel().setBackground(lastStateFrom.getColor());
        from.setName(lastStateFrom.getName());
        from.setColor(lastStateFrom.getColor());
        from.setForce(lastStateFrom.getForce());
        
        isMyTurn = true;
    }

    private void sendMessage() {
        String message = name + ": ";

        if (firstConfigurationFinished) {

            message += messageToSend.getText();
            prtStream.println(message);
            prtStream.flush();

            receivedMessage.setText(receivedMessage.getText() + message + "\n");
        } else {
            showMessageDialog(null, "Você só poderá enviar mensagens depois de escolher sua cor", "", INFORMATION_MESSAGE);
        }
        messageToSend.setText("");
        messageToSend.setCaretPosition(0);

    }

    private void setInitialGameState() {
        if (color.equals("red")) {
            redChoose.setEnabled(true);
            blueChoose.setVisible(false);
            redChoose.setText("Reiniciar");
        } else {
            blueChoose.setEnabled(true);
            redChoose.setVisible(false);
            blueChoose.setText("Reiniciar");
        }
        for (int i = 0; i < pieces.size(); i++) {
            JLabel movimentLabel = pieces.get(i).getLabel();
            if (i < 40) {
                movimentLabel.setOpaque(true);
                if (color.equals("red")) {
                    movimentLabel.setBackground(Color.BLUE);
                } else {
                    movimentLabel.setBackground(Color.RED);
                }
            }
            if (i > 59) {
                movimentLabel.setOpaque(true);
                if (color.equals("red")) {
                    movimentLabel.setBackground(Color.RED);
                } else {
                    movimentLabel.setBackground(Color.BLUE);
                }
            }
        }
    }

    private boolean verifyTurnValidation(String[] comands) {
        showMessageDialog(null, comands[2] + " avançou da posição " + comands[4] + " para a posição " + comands[6], "", INFORMATION_MESSAGE);
        int resposta = showConfirmDialog(null, "Você aceita esse movimento?", "", JOptionPane.YES_NO_OPTION);
        if (resposta == JOptionPane.YES_OPTION) {
            logs.setText(logs.getText() + comands[2] + " avançou da posição " + comands[4] + " para a posição " + comands[6] + "\n");
            updateMyScreen(Integer.parseInt(comands[4]), Integer.parseInt(comands[6]));
            return true;
        } else {
            isMyTurn = false;
            return false;
        }
    }
    
    private void updateMyScreen(int from, int to){
        
        Color enemyColor = color.equals("red") ? Color.BLUE : Color.RED;
        
        pieces.get(to).getLabel().setIcon(pieces.get(from).getLabel().getIcon());
        pieces.get(to).getLabel().setBackground(enemyColor);
        pieces.get(to).getLabel().setOpaque(true);
        
        pieces.get(from).getLabel().setIcon(null);
        pieces.get(from).getLabel().setBackground(null);
        pieces.get(from).getLabel().setOpaque(false);
        
    }

    private void sendComand(String piece, String position) {
        String comand = gamePatern + piece + position;

        prtStream.println(comand);
        prtStream.flush();
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel background;
    private javax.swing.JButton blueChoose;
    private javax.swing.JLabel captainChoose;
    private javax.swing.JLabel captainLabelDescription;
    private javax.swing.JLabel colonelChoose;
    private javax.swing.JLabel colonelLabelDescription;
    private javax.swing.JLabel corporalChoose;
    private javax.swing.JLabel corporalLabelDescription;
    private javax.swing.JButton finishTurn1;
    private javax.swing.JLabel flagChoose;
    private javax.swing.JLabel flagLabelDescription;
    private javax.swing.JLabel generalChoose;
    private javax.swing.JLabel generalLabelDescription;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel label0;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel label10;
    private javax.swing.JLabel label11;
    private javax.swing.JLabel label12;
    private javax.swing.JLabel label13;
    private javax.swing.JLabel label14;
    private javax.swing.JLabel label15;
    private javax.swing.JLabel label16;
    private javax.swing.JLabel label17;
    private javax.swing.JLabel label18;
    private javax.swing.JLabel label19;
    private javax.swing.JLabel label2;
    private javax.swing.JLabel label20;
    private javax.swing.JLabel label21;
    private javax.swing.JLabel label22;
    private javax.swing.JLabel label23;
    private javax.swing.JLabel label24;
    private javax.swing.JLabel label25;
    private javax.swing.JLabel label26;
    private javax.swing.JLabel label27;
    private javax.swing.JLabel label28;
    private javax.swing.JLabel label29;
    private javax.swing.JLabel label3;
    private javax.swing.JLabel label30;
    private javax.swing.JLabel label31;
    private javax.swing.JLabel label32;
    private javax.swing.JLabel label33;
    private javax.swing.JLabel label34;
    private javax.swing.JLabel label35;
    private javax.swing.JLabel label36;
    private javax.swing.JLabel label37;
    private javax.swing.JLabel label38;
    private javax.swing.JLabel label39;
    private javax.swing.JLabel label4;
    private javax.swing.JLabel label40;
    private javax.swing.JLabel label41;
    private javax.swing.JLabel label42;
    private javax.swing.JLabel label43;
    private javax.swing.JLabel label44;
    private javax.swing.JLabel label45;
    private javax.swing.JLabel label46;
    private javax.swing.JLabel label47;
    private javax.swing.JLabel label48;
    private javax.swing.JLabel label49;
    private javax.swing.JLabel label5;
    private javax.swing.JLabel label50;
    private javax.swing.JLabel label51;
    private javax.swing.JLabel label52;
    private javax.swing.JLabel label53;
    private javax.swing.JLabel label54;
    private javax.swing.JLabel label55;
    private javax.swing.JLabel label56;
    private javax.swing.JLabel label57;
    private javax.swing.JLabel label58;
    private javax.swing.JLabel label59;
    private javax.swing.JLabel label6;
    private javax.swing.JLabel label60;
    private javax.swing.JLabel label61;
    private javax.swing.JLabel label62;
    private javax.swing.JLabel label63;
    private javax.swing.JLabel label64;
    private javax.swing.JLabel label65;
    private javax.swing.JLabel label66;
    private javax.swing.JLabel label67;
    private javax.swing.JLabel label68;
    private javax.swing.JLabel label69;
    private javax.swing.JLabel label7;
    private javax.swing.JLabel label70;
    private javax.swing.JLabel label71;
    private javax.swing.JLabel label72;
    private javax.swing.JLabel label73;
    private javax.swing.JLabel label74;
    private javax.swing.JLabel label75;
    private javax.swing.JLabel label76;
    private javax.swing.JLabel label77;
    private javax.swing.JLabel label78;
    private javax.swing.JLabel label79;
    private javax.swing.JLabel label8;
    private javax.swing.JLabel label80;
    private javax.swing.JLabel label81;
    private javax.swing.JLabel label82;
    private javax.swing.JLabel label83;
    private javax.swing.JLabel label84;
    private javax.swing.JLabel label85;
    private javax.swing.JLabel label86;
    private javax.swing.JLabel label87;
    private javax.swing.JLabel label88;
    private javax.swing.JLabel label89;
    private javax.swing.JLabel label9;
    private javax.swing.JLabel label90;
    private javax.swing.JLabel label91;
    private javax.swing.JLabel label92;
    private javax.swing.JLabel label93;
    private javax.swing.JLabel label94;
    private javax.swing.JLabel label95;
    private javax.swing.JLabel label96;
    private javax.swing.JLabel label97;
    private javax.swing.JLabel label98;
    private javax.swing.JLabel label99;
    private javax.swing.JLabel lietenantLabelDescription;
    private javax.swing.JLabel liutenantChoose;
    private javax.swing.JTextArea logs;
    private javax.swing.JLabel majorChoose;
    private javax.swing.JLabel majorLabelDescription;
    private javax.swing.JTextArea messageToSend;
    private javax.swing.JLabel mineChoose;
    private javax.swing.JLabel mineLabelDescription;
    private javax.swing.JTextArea receivedMessage;
    private javax.swing.JButton redChoose;
    private javax.swing.JButton sendButton;
    private javax.swing.JLabel sergeantChoose;
    private javax.swing.JLabel sergeantLabelDescription;
    private javax.swing.JLabel soldierChoose;
    private javax.swing.JLabel soldierLabelDescription;
    private javax.swing.JLabel spyChoose;
    private javax.swing.JLabel spyLabelDescription;
    private javax.swing.JLabel subLiutenantChoose;
    private javax.swing.JLabel subLiutenantLabelDescription;
    // End of variables declaration//GEN-END:variables
}
